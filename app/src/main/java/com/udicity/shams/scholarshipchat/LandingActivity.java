package com.udicity.shams.scholarshipchat;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

public class LandingActivity extends AppCompatActivity {

    private Button mLoginButton;
    private Button mSignUpButton;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firebaseAuth = FirebaseAuth.getInstance();
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null){
                    startActivity(new Intent(LandingActivity.this , MainActivity.class));
                    finish();
                }
            }
        };

        setContentView(R.layout.landing_activity);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        mLoginButton = findViewById(R.id.btn_sign_in_landing_activity);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LandingActivity.this , LoginActivity.class));
                finish();
            }
        });

        mSignUpButton = findViewById(R.id.btn_sign_up_landing_activity);
        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(LandingActivity.this , SignUpActivity.class));
               finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        firebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAuthStateListener != null){
            firebaseAuth.removeAuthStateListener(mAuthStateListener);
        }
    }
}
